import Vue from "vue";
import Router from "vue-router";
import goods from "@/views/goods/goods.vue";


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/goods",
      name: "goods",
      component: goods
    },
    {
      path: "/seller",
      name: "seller",
       component: () =>
       import(/* webpackChunkName: "seller" */"@/views/seller/seller.vue")
    },
    {
      path: "/ratings",
      name: "ratings",
      //动态导入
      component: () =>
        import(/* webpackChunkName: "ratings" */"@/views/ratings/ratings.vue")
    },
    {
      path: "/xq",
      name: "xq",
      //动态导入
      component: () =>
        import(/* webpackChunkName: "xq" */"@/views/xq/xq.vue")
    },
    {
      path: "/myCar",
      name: "myCar",
      //动态导入
      component: () =>
        import(/* webpackChunkName: "myCar" */"@/views/myCar/myCar.vue")
    },
    {
      path: "/show",
      name: "show",
      //动态导入
      component: () =>
        import(/* webpackChunkName: "show" */"@/views/show/show.vue")
    },
    {
      path: "/order",
      name: "order",
      //动态导入
      component: () =>
        import(/* webpackChunkName: "order" */"@/views/order/order.vue")
    },
    {
      path: "/queOrder",
      name: "queOrder",
      //动态导入
      component: () =>
        import(/* webpackChunkName: "queOrder" */"@/views/queOrder/queOrder.vue")
    },
    {
      path: "/my",
      name: "my",
      //动态导入
      component: () =>
        import(/* webpackChunkName: "my" */"@/views/my/my.vue")
    },
    {
      path: "/",
      redirect: '/goods'
    },
    {
      path: "*",
      redirect: '/goods'
    },
  ]
});



