import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Router from 'vue-router'
const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  undefined
  return routerPush.call(this, location).catch(error => error)
}
// Vue.prototype.$http=axios

router.beforeEach((to,form,next)=>{
  if(to.path=='/order'){
    let token=localStorage.getItem('token')
    if(!token){
      next('/goods')
    }

  }
  next()
})

Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
