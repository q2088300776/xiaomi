import axios from 'axios'
// 1.创建axios的实例
const instance = axios.create({
    //本地json
    baseURL: process.env.VUE_APP_BASE_API,
 
    timeout: 5000,


});

// 配置请求拦截器-常见的就是统一加入请求头
instance.interceptors.request.use(
    config => {
        // console.log('发送中', config); //config就是axios里返回的promise对象
        return config;
    },
    err => {
        console.log('发送失败');
        return err;
    }
);

//配置响应拦截器——对响应数据做些事,常见的就是设置一些统一错误弹窗
instance.interceptors.response.use(
    response => {
        // console.log('响应成功', response);
        return response.data;
    },
    err => {
        if (err && err.response) {
            switch (err.response.status) {
                case 400:
                    err.message = "请求错误";
                    break;
                case 401:
                    err.message = "未授权的访问";
                    break;
            }
        }
        // console.log('响应失败1');
        return err;

    }
);

export default instance