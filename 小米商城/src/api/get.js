import http from "./request.js"

// 轮播图数据
async function getSwiperdata() {
    var res = await http({
        url: 'resources/carousel',
        method: 'post'
    })
    return res
}

// 手机数据
async function getPromo() {
    var res = await http({
        url: 'product/getPromoProduct',
        method: 'post',
        data: { categoryName: '手机' }
    })
    return res
}

// 家电数据
async function getHot() {
    var res = await http({
        url: 'product/getHotProduct',
        method: 'post',
        data: { categoryName: ["电视机", "空调", "洗衣机"] }
    })
    return res
}

// 配件数据
async function getTao() {
    var res = await http({
        url: 'product/getHotProduct',
        method: 'post',
        data: { categoryName: ["保护套", "保护膜", "充电器", "充电宝"] }
    })
    return res
}


// 电视
async function getTeleplay() {
    var res = await http({
        url: 'product/getPromoProduct',
        method: 'post',
        data: { categoryName: '电视机' }
    })
    return res
}

// 保护套
async function getProtective() {
    var res = await http({
        url: 'product/getPromoProduct',
        method: 'post',
        data: { categoryName: '保护套' }
    })
    return res
}


// 充电器
async function getCharger() {
    var res = await http({
        url: 'product/getPromoProduct',
        method: 'post',
        data: { categoryName: '充电器' }
    })
    return res
}



// 分类
async function getClassify() {
    var res = await http({
        url: 'product/getCategory',
        method: 'post',
    })
    return res
}

// 全部数据
async function getAll() {
    var res = await 
    http({
        url: 'product/getAllProduct',
        method: 'post',
    })
    return res
}

// 详情
async function getDetails(msg) {
    var res = await http({
        url: 'product/getDetails',
        method: 'post',
        data:{productID: msg}
    })
    return res
}


// 详情轮播
async function getDetailsPicture(msg) {
    var res = await http({
        url: 'product/getDetailsPicture',
        method: 'post',
        data:{productID: msg}
    })
    return res
}

// 注册
async function getZhuce(msg) {
    var res = await http({
        url: 'users/register',
        method: 'post',
        data:{userName: msg.userName,password:msg.password}
    })
    return res
}


// 注册用户名是否重复
async function getZhuceName(msg) {
    var res = await http({
        url: 'users/findUserName',
        method: 'post',
        data:{productID: msg}
    })
    return res
}

// 登录
async function login(msg) {
    var res = await http({
        url: 'users/login',
        method: 'post',
        data:{userName: msg.userName,password:msg.password}
    })
    return res
}

// 添加到购物车
async function addCar(msg) {
    var res = await http({
        url: 'user/shoppingCart/addShoppingCart',
        method: 'post',
        data:{user_id: msg.user_id,product_id:msg.product_id}
    })
    return res
}

// 获取购物车数据
async function getCar(msg) {
    var res = await http({
        url: 'user/shoppingCart/getShoppingCart',
        method: 'post',
        data:{user_id: msg}
    })
    return res
}

// 修改购物车数据
async function changeNum(msg) {
    var res = await http({
        url: 'user/shoppingCart/updateShoppingCart',
        method: 'post',
        data:{user_id:msg.user_id,num:msg.num,product_id:msg.product_id}
    })
    return res
}

// 删除购物车数据
async function delCar(msg) {
    var res = await http({
        url: 'user/shoppingCart/deleteShoppingCart',
        method: 'post',
        data:{user_id:msg.user_id,product_id:msg.product_id}
    })
    return res
}


// 收藏数据
async function getShou(msg) {
    var res = await http({
        url: 'user/collect/getCollect',
        method: 'post',
        data:{user_id:msg}
    })
    return res
}

// 添加收藏数据
async function addShou(msg) {
    var res = await http({
        url: 'user/collect/addCollect',
        method: 'post',
        data:{user_id:msg.user_id,product_id:msg.product_id}
    })
    return res
}

// 删除收藏数据
async function delShou(msg) {
    var res = await http({
        url: 'user/collect/deleteCollect',
        method: 'post',
        data:{user_id:msg.user_id,product_id:msg.product_id}
    })
    return res
}

// 添加订单
async function addOrder(msg) {
    var res = await http({
        url: 'user/order/addOrder',
        method: 'post',
        data:{user_id:msg.user_id,products:msg.products}
    })
    return res
}

// 获取订单数据
async function getOrder(msg) {
    var res = await http({
        url: 'user/order/getOrder',
        method: 'post',
        data:msg
    })
    return res
}

// 获取订单数据
async function getProduct(msg) {
    var res = await http({
        url: 'product/getProductByCategory',
        method: 'post',
        data:{categoryID:msg,currentPage:1,pageSize:15}
    })
    return res
}


// 获取订单数据
async function getSearch(msg) {
    var res = await http({
        url: 'product/getProductBySearch',
        method: 'post',
        data:{search:msg,currentPage:1,pageSize:15}
    })
    return res
}

// 获取关于
async function getMy(msg) {
    var res = await http({
        url: 'public/docs/README.md',
        method: 'get',
        data:msg
    })
    return res
}


export { getSwiperdata, getPromo, getHot, getTao, getTeleplay, getProtective, getCharger,
    getClassify,getAll,getDetails,getDetailsPicture ,getZhuce,getZhuceName,login,addCar,getCar,
    changeNum,delCar,getShou,addShou,delShou,addOrder,getOrder,getProduct,getSearch,getMy
}